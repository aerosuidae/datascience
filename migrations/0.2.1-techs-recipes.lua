local check = function(force, techname)
  if force.technologies[techname] ~= nil and force.technologies[techname].researched then
    force.technologies[techname].researched = false
    force.technologies[techname].researched = true
  end
end

for i, force in pairs(game.forces) do
  force.reset_recipes()
  force.reset_technologies()
  check(force, "datascience-tech-collection")
  check(force, "datascience-tech-computation")
  check(force, "datascience-tech-analysis")
end
