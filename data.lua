local enabled = false
local group = "datascience-items"
local subgroup = "datascience-data"
local order = "a"
local icon_datum = "__datascience__/icon-datum.png"
local icon_datum_size = 32
local icon_punchcard = "__datascience__/icon-punchcard.png"
local icon_punchcard_size = 32
local icon_data_record = "__datascience__/icon-data-record.png"
local icon_data_record_size = 32
local icon_data_clean = "__datascience__/icon-data-clean.png"
local icon_data_clean_size = 32
local icon_data_analyzed = "__datascience__/icon-data-analyzed.png"
local icon_data_analyzed_size = 32
local icon_collector = "__datascience__/icon-collector.png"
local icon_collector_size = 32
local icon_computer = "__datascience__/icon-computer.png"
local icon_computer_size = 32
local icon_rack = "__datascience__/icon-rack.png"
local icon_rack_size = 32
local data_stack_size = 100

data:extend({
  {
    type = "recipe-category",
    name = "datascience-collection",
  },
  {
    type = "recipe-category",
    name = "datascience-computation",
  },
  {
    type = "recipe-category",
    name = "datascience-analysis",
  },
  {
    type = "item-group",
    name = group,
    order = "z",
    inventory_order = "z",
    icon = "__datascience__/tech-collection.png",
    icon_size = 128,
  },
  {
    type = "item-subgroup",
    name = subgroup,
    group = group,
    order = "z"
  },
  {
    type = "technology",
    name = "datascience-tech-collection",
    icon = "__datascience__/tech-collection.png",
    icon_size = 128,
    effects = {
      { type = "unlock-recipe", recipe = "datascience-make-punchcards" },
      { type = "unlock-recipe", recipe = "datascience-data-input" },
      --{ type = "unlock-recipe", recipe = "datascience-data-reuse" },
      { type = "unlock-recipe", recipe = "datascience-sensor" },
      { type = "unlock-recipe", recipe = "datascience-collector" },
    },
    prerequisites = {
      "circuit-network",
      "advanced-electronics",
      "electric-energy-distribution-1",
    },
    unit = {
      count = 150,
      ingredients = {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1}
      },
      time = 30
    },
    order = "c-o-a",
  },
  {
    type = "technology",
    name = "datascience-tech-computation",
    icon = "__datascience__/tech-computation.png",
    icon_size = 128,
    effects = {
      { type = "unlock-recipe", recipe = "datascience-data-cleanse" },
      { type = "unlock-recipe", recipe = "datascience-computer" },
    },
    prerequisites = {
      "datascience-tech-collection",
      "automation-2",
      "speed-module",
    },
    unit = {
      count = 150,
      ingredients = {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"chemical-science-pack", 1},
      },
      time = 45
    },
    order = "c-o-a",
  },
  {
    type = "technology",
    name = "datascience-tech-analysis",
    icon = "__datascience__/tech-analysis.png",
    icon_size = 128,
    effects = {
      { type = "unlock-recipe", recipe = "datascience-data-analyze" },
      { type = "unlock-recipe", recipe = "datascience-rack" },
    },
    prerequisites = {
      "datascience-tech-computation",
      "automation-3",
    },
    unit = {
      count = 150,
      ingredients = {
        {"automation-science-pack", 1},
        {"logistic-science-pack", 1},
        {"chemical-science-pack", 1},
        {"production-science-pack", 1},
      },
      time = 60
    },
    order = "c-o-a",
  },
  {
    type = "item",
    name = "datascience-datum",
    icon = icon_datum,
    icon_size = icon_datum_size,
    subgroup = subgroup,
    order = order .. "-a[datascience-datum]",
    stack_size = data_stack_size,
  },
  {
    type = "item",
    name = "datascience-punchcard",
    icon = icon_punchcard,
    icon_size = icon_punchcard_size,
    subgroup = subgroup,
    order = order .. "-a[datascience-punchcard]",
    stack_size = data_stack_size,
  },
  {
    type = "item",
    name = "datascience-data-record",
    icon = icon_data_record,
    icon_size = icon_data_record_size,
    subgroup = subgroup,
    order = order .. "-b[datascience-data-record]",
    stack_size = data_stack_size,
  },
  {
    type = "item",
    name = "datascience-data-clean",
    icon = icon_data_clean,
    icon_size = icon_data_clean_size,
    subgroup = subgroup,
    order = order .. "-c[datascience-data-clean]",
    stack_size = data_stack_size,
  },
  {
    type = "item",
    name = "datascience-data-analyzed",
    icon = icon_data_analyzed,
    icon_size = icon_data_analyzed_size,
    subgroup = subgroup,
    order = order .. "-d[datascience-data-analyzed]",
    stack_size = data_stack_size,
  },
  {
    type = "item",
    name = "datascience-sensor",
    icon = "__datascience__/icon-sensor.png",
    icon_size = 32,
    subgroup = subgroup,
    place_result = "datascience-sensor",
    order = order .. "-e[datascience-sensor]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-collector",
    icon = icon_collector,
    icon_size = icon_collector_size,
    subgroup = subgroup,
    place_result = "datascience-collector",
    order = order .. "-f[datascience-collector]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-collector-radar",
    icon = "__base__/graphics/icons/radar.png",
    icon_size = 32,
    subgroup = subgroup,
    place_result = "datascience-collector-radar",
    order = order .. "-f[datascience-collector-radar]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-computer",
    icon = icon_computer,
    icon_size = icon_computer_size,
    subgroup = subgroup,
    place_result = "datascience-computer",
    order = order .. "-g[datascience-computer]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-computer-light",
    icon = "__base__/graphics/icons/rail-signal.png",
    icon_size = 32,
    subgroup = subgroup,
    place_result = "datascience-computer-light",
    order = order .. "-h[datascience-computer-light]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-rack",
    icon = icon_rack,
    icon_size = icon_rack_size,
    subgroup = subgroup,
    place_result = "datascience-rack",
    order = order .. "-i[datascience-rack]",
    stack_size = 50,
  },
  {
    type = "item",
    name = "datascience-rack-display",
    icon = "__base__/graphics/icons/radar.png",
    icon_size = 32,
    subgroup = subgroup,
    place_result = "datascience-rack-display",
    order = order .. "-f[datascience-rack-display]",
    stack_size = 50,
  },
  {
    type = "recipe",
    name = "datascience-make-punchcards",
    category = "advanced-crafting",
    subgroup = subgroup,
    enabled = enabled,
    icon = icon_punchcard,
    icon_size = icon_punchcard_size,
    ingredients = {
      { type = "item", name = "wood", amount = 1 },
      { type = "fluid", name = "water", amount = 10 },
    },
    results = {
      { type = "item", name = "datascience-punchcard", amount = 10 },
    },
    hidden = false,
    energy_required = 1.0,
    order = order .. "a[datascience-make-punchcards]",
  },
  {
    type = "recipe",
    name = "datascience-data-input",
    result = "datascience-data-record",
    category = "datascience-collection",
    subgroup = subgroup,
    enabled = enabled,
    icon = icon_data_record,
    icon_size = icon_data_record_size,
    ingredients = {
      { "datascience-datum", 25 },
      { "datascience-punchcard", 1 },
    },
    hidden = false,
    energy_required = 1,
    order = order .. "b[datascience-data-input]",
  },
  {
    type = "recipe",
    name = "datascience-data-cleanse",
    category = "datascience-computation",
    enabled = enabled,
    subgroup = subgroup,
    icon = icon_data_clean,
    icon_size = icon_data_clean_size,
    ingredients = {
      { "datascience-data-record", 10 },
      { "datascience-punchcard", 1 },
      { type = "fluid", name = "water", amount = 10 },
    },
    results = {
      { type = "item", name = "datascience-data-clean", amount = 1 },
      { type = "fluid", name = "steam", amount = 10, temperature = 165 },
    },
    hidden = false,
    energy_required = 5,
    order = order .. "c[datascience-data-cleanse]",
  },
  {
    type = "recipe",
    name = "datascience-data-analyze",
    category = "datascience-analysis",
    enabled = enabled,
    subgroup = subgroup,
    icon = icon_data_analyzed,
    icon_size = icon_data_analyzed_size,
    ingredients = {
      { "datascience-data-clean", 4 },
      { "datascience-punchcard", 1 },
      { type = "fluid", name = "water", amount = 100 },
    },
    results = {
      { type = "item", name = "datascience-data-analyzed", amount = 1 },
      { type = "fluid", name = "steam", amount = 100, temperature = 165 },
    },
    hidden = false,
    energy_required = 25,
    order = order .. "d[datascience-data-analyze]",
  },
  {
    type = "recipe",
    name = "datascience-sensor",
    result = "datascience-sensor",
    enabled = enabled,
    icon = "__datascience__/icon-sensor.png",
    icon_size = 32,
    ingredients = {
      { "advanced-circuit", 1 },
      { "constant-combinator", 1 },
      { "medium-electric-pole", 1 },
    },
    energy_required = 1,
    order = order .. "e[datascience-data-analyze]",
  },
  {
    type = "recipe",
    name = "datascience-collector",
    result = "datascience-collector",
    enabled = enabled,
    icon = icon_collector,
    icon_size = 32,
    ingredients = {
      { "radar", 1 },
      { "advanced-circuit", 1 },
      { "assembling-machine-1", 1 },
    },
    energy_required = 2,
    order = order .. "f[datascience-collector]",
  },
  {
    type = "recipe",
    name = "datascience-computer",
    result = "datascience-computer",
    enabled = enabled,
    icon = icon_computer,
    icon_size = icon_computer_size,
    ingredients = {
      { "speed-module", 1 },
      { "arithmetic-combinator", 1 },
      { "decider-combinator", 1 },
      { "assembling-machine-2", 1 },
    },
    energy_required = 4,
    order = order .. "g[datascience-computer]",
  },
  {
    type = "recipe",
    name = "datascience-rack",
    result = "datascience-rack",
    enabled = enabled,
    icon = icon_rack,
    icon_size = icon_rack_size,
    ingredients = {
      { "red-wire", 12 },
      { "green-wire", 12 },
      { "datascience-computer", 1 },
      { "assembling-machine-3", 1 },
    },
    energy_required = 4,
    order = order .. "h[datascience-rack]",
  },
})

local chest = table.deepcopy(data.raw["container"]["iron-chest"])

local sensor = table.deepcopy(data.raw["beacon"]["beacon"])
sensor.name = "datascience-sensor"
sensor.place_result = "datascience-sensor"
sensor.minable.result = "datascience-sensor"
sensor.allowed_effects = {}
sensor.collision_box = chest.collision_box
sensor.drawing_box = chest.drawing_box
sensor.selection_box = chest.selection_box
sensor.energy_usage = "10kW"
sensor.module_specification.module_slots = 0
sensor.icon = "__datascience__/icon-sensor.png"
sensor.icon_size = 32

sensor.animation = {
  animation_speed = 0.5,
  filename = "__base__/graphics/entity/beacon/beacon-antenna.png",
  frame_count = 32,
  height = 50,
  line_length = 8,
  shift = {
    -0.03125*0.3,
    -1.71875*0.3,
  },
  width = 54,
  scale = 0.3,
}

sensor.animation_shadow = {
  animation_speed = 0.5,
  filename = "__base__/graphics/entity/beacon/beacon-antenna-shadow.png",
  frame_count = 32,
  height = 49,
  line_length = 8,
  shift = {
    3.140625*0.3,
    0.484375*0.3,
  },
  width = 63,
  scale = 0.3,
}

sensor.base_picture = {
  filename = "__base__/graphics/entity/beacon/beacon-base.png",
  height = 93,
  shift = {
    0.34375*0.3,
    0.046875*0.3,
  },
  width = 116,
  scale = 0.3,
}

data:extend({ sensor })

local collector = table.deepcopy(data.raw["assembling-machine"]["assembling-machine-2"])
collector.name = "datascience-collector"
collector.place_result = "datascience-collector"
collector.minable.result = "datascience-collector"
collector.crafting_categories = {
  "datascience-collection",
}
collector.fixed_recipe = "datascience-data-input"
collector.crafting_speed = 1

data:extend({ collector })

data:extend({
  {
    type = "simple-entity-with-force",
    name = "datascience-collector-radar",
    flags = {"player-creation", "not-blueprintable", "not-deconstructable", "placeable-off-grid"},
    selectable_in_game = false,
    mined_sound = nil,
    minable = nil,
    collision_box = nil,
    selection_box = nil,
    collision_mask = {},
    animations = {
      {
        filename = "__base__/graphics/entity/radar/radar.png",
        height = 128,
        width = 98,
        frame_count = 64,
        line_length = 8,
        priority = "low",
        scale = 0.4,
      },
      {
        filename = "__base__/graphics/entity/radar/radar.png",
        height = 128,
        width = 98,
        frame_count = 1,
        line_length = 1,
        priority = "low",
        scale = 0.4,
      },
    },
    render_layer = "higher-object-under",
    tile_width = 1,
    tile_height = 1,
  },
})

local computer = table.deepcopy(data.raw["assembling-machine"]["assembling-machine-2"])
computer.name = "datascience-computer"
computer.place_result = "datascience-computer"
computer.minable.result = "datascience-computer"
computer.crafting_categories = {
  "datascience-computation",
}
computer.allowed_effects = {
	"speed",
	"consumption",
}
computer.module_specification = {
	module_slots = 0,
}
computer.crafting_speed = 1

data:extend({ computer })

data:extend({
  {
    type = "simple-entity-with-force",
    name = "datascience-computer-light",
    flags = {"player-creation", "not-blueprintable", "not-deconstructable", "placeable-off-grid"},
    selectable_in_game = false,
    mined_sound = nil,
    minable = nil,
    collision_box = nil,
    selection_box = nil,
    collision_mask = {},
    animations = {
      {
        filename = "__datascience__/lights.png",
        height = 96,
        width = 96,
        frame_count = 3,
        line_length = 3,
        priority = "low",
        animation_speed = 0.1,
        scale = 0.5,
      },
      {
        filename = "__datascience__/lights.png",
        height = 96,
        width = 96,
        frame_count = 1,
        line_length = 1,
        priority = "low",
        animation_speed = 0.1,
        scale = 0.5,
      },
    },
    render_layer = "higher-object-under",
    tile_width = 1,
    tile_height = 1,
  },
})

local rack = table.deepcopy(data.raw["assembling-machine"]["assembling-machine-3"])
rack.name = "datascience-rack"
rack.place_result = "datascience-rack"
rack.minable.result = "datascience-rack"
rack.energy_usage = computer.energy_usage
rack.crafting_categories = {
  "datascience-computation",
  "datascience-analysis",
}
rack.allowed_effects = {
	"speed",
	"consumption",
}
rack.module_specification = {
	module_slots = 12,
}
rack.crafting_speed = 1

data:extend({ rack })

data:extend({
  {
    type = "simple-entity-with-force",
    name = "datascience-rack-display",
    flags = {"player-creation", "not-blueprintable", "not-deconstructable", "placeable-off-grid"},
    selectable_in_game = false,
    mined_sound = nil,
    minable = nil,
    collision_box = nil,
    selection_box = nil,
    collision_mask = {},
    animations = {
      {
        filename = "__base__/graphics/entity/combinator/hr-combinator-displays.png",
        height = 22,
        width = 30,
        frame_count = 18,
        line_length = 6,
        priority = "low",
        scale = 1.0,
      },
      {
        filename = "__base__/graphics/entity/combinator/hr-combinator-displays.png",
        height = 22,
        width = 30,
        frame_count = 1,
        line_length = 1,
        priority = "low",
        scale = 1.0,
      },
    },
    render_layer = "higher-object-under",
    tile_width = 1,
    tile_height = 1,
  },
})
