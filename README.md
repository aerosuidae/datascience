# Factorio Mod: Data Science

*Alpha code and quite unbalanced! Feedback welcome.*

Adds data science elements to blue, purple and yellow science packs, requiring collection, cleansing and analysis of production data across the entire base.

## Entities

* Sensors to collect data from crafting machines.
* Collectors to receive and aggregate raw data records on punch cards.
* Computers to perform data cleansing.
* Racks 12U to perform data analysis (and optionally faster cleansing).

## Items

* *Punch Cards* for storing data.
* *Raw Data* added to blue science recipe.
* *Clean Data* added to purple science recipe.
* *Analysed Data* added to yellow science recipe.

## Technologies

* Data Collection (red + green)
* Data Computation (red + green + blue)
* Data Analysis (red + green + blue + purple)

## Basic Approach

1. Build a normal starter base for red and green science, and the usual engines + mining-drill arrays in preparation for blue science.
1. Craft a bunch of *sensors* and cover all available assemblers, refineries, chemical plants, and furnaces. Just like beacons, sensors have a yellow area of effect overlay. Note that furnaces won't be selection-highlighted but they're still counted by the mod.
1. Craft a *collector* or two. Supply these with *punch cards* and *raw data records* should start to flow. Commence blue science.

## Water Cooling

Computers and Racks require water cooling and produce a bit of steam. Hook up steam engines to reclaim some energy!

## Some Vague Aims...

* Link science output to overall base production.
* Make science a bit more interesting with a different game mechanic.
* Complicate just about every blueprint in existence with sensors!
* Replace, or make more indirect, the requirement for intermediate items in higher science pack recipes.
* Think about ways to link this stuff into the circuit network a bit more in the future.
* Think of a way to add a higher-level computer that must be laid out using more than one entity -- Cluster? Cooling?