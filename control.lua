
function EnsureState()
  local g = global
  if g.sensor_map == nil then
    g.sensor_map = g.sensor_map or {}
    g.sensor_queue = g.sensor_queue or {}
    g.sensor_index = g.sensor_index or 1
    g.sensor_targets = g.sensor_targets or {}
  end
  if g.collector_map == nil then
    g.collector_map = g.collector_map or {}
    g.collector_queue = g.collector_queue or {}
    g.collector_index = g.collector_index or 1
    g.collector_radars = g.collector_radars or {}
  end
  if g.computer_map == nil then
    g.computer_map = g.computer_map or {}
    g.computer_lights = g.computer_lights or {}
    g.computer_queue = g.computer_queue or {}
    g.computer_index = g.computer_index or 1
  end
  if g.rack_map == nil then
    g.rack_map = g.rack_map or {}
    g.rack_displays = g.rack_displays or {}
    g.rack_queue = g.rack_queue or {}
    g.rack_index = g.rack_index or 1
    g.data_points = g.data_points or 0
  end
end


function SensorTargets(sensor)
  local position = sensor.position

  return sensor.surface.find_entities_filtered({
    area = {
      left_top = { position.x - 3, position.y - 3 },
      right_bottom = { position.x + 4, position.y + 4 },
    },
    type = {
      "assembling-machine",
      "furnace",
    },
  })
end

function OnEntityCreated(event)
  local g = global
  EnsureState()

  if event.created_entity.name == "datascience-sensor" then
    local entity = event.created_entity
    entity.operable = false
    g.sensor_map[entity.unit_number] = entity
    table.insert(g.sensor_queue, entity)
  end

  if event.created_entity.name == "datascience-collector" then
    local entity = event.created_entity
    g.collector_map[entity.unit_number] = entity
    table.insert(g.collector_queue, entity)

    local radar = entity.surface.create_entity({
      name = "datascience-collector-radar",
      position = { x = entity.position.x+0.35, y = entity.position.y-0.25 },
      force = entity.force,
    })

    radar.operable = false
    radar.minable = false
    radar.destructible = false

    g.collector_radars[entity.unit_number] = radar
  end

  if event.created_entity.name == "datascience-computer" then
    local entity = event.created_entity
    g.computer_map[entity.unit_number] = entity
    table.insert(g.computer_queue, entity)

    local light = entity.surface.create_entity({
      name = "datascience-computer-light",
      position = { x = entity.position.x+0.5, y = entity.position.y-1.4 },
      force = entity.force,
    })

    light.operable = false
    light.minable = false
    light.destructible = false

    g.computer_lights[entity.unit_number] = light
  end

  if event.created_entity.name == "datascience-rack" then
    local entity = event.created_entity
    g.rack_map[entity.unit_number] = entity
    table.insert(g.rack_queue, entity)

    local display = entity.surface.create_entity({
      name = "datascience-rack-display",
      position = { x = entity.position.x-0.5, y = entity.position.y+0.2 },
      force = entity.force,
    })

    display.operable = false
    display.minable = false
    display.destructible = false

    g.rack_displays[entity.unit_number] = display
  end
end

function OnEntityRemoved(event)
  EnsureState()
  local g = global

  if event.entity.name == "datascience-sensor" then
    table.remove(g.sensor_map, event.entity.unit_number)
    for i, entity in ipairs(g.sensor_queue) do
      if entity.unit_number == event.entity.unit_number then
        table.remove(g.sensor_queue, i)
        break
      end
    end
    for i, target in ipairs(SensorTargets(event.entity)) do
      table.remove(g.sensor_targets, target.unit_number)
    end
  end

  if event.entity.name == "datascience-collector" then
    table.remove(g.collector_map, event.entity.unit_number)
    for i, entity in ipairs(g.collector_queue) do
      if entity.unit_number == event.entity.unit_number then
        table.remove(g.collector_queue, i)
        break
      end
    end
    g.collector_radars[event.entity.unit_number].destroy()
  end

  if event.entity.name == "datascience-computer" then
    table.remove(g.computer_map, event.entity.unit_number)
    for i, entity in ipairs(g.computer_queue) do
      if entity.unit_number == event.entity.unit_number then
        table.remove(g.computer_queue, i)
        break
      end
    end
    g.computer_lights[event.entity.unit_number].destroy()
  end

  if event.entity.name == "datascience-rack" then
    table.remove(g.rack_map, event.entity.unit_number)
    for i, entity in ipairs(g.rack_queue) do
      if entity.unit_number == event.entity.unit_number then
        table.remove(g.rack_queue, i)
        break
      end
    end
    g.rack_displays[event.entity.unit_number].destroy()
  end

  if event.entity.type == "assembling-machine" or event.entity.type == "furnace" then
    table.remove(g.sensor_targets, event.entity.unit_number)
  end

  table.remove(g.sensor_targets, event.entity.unit_number)
end

function OnTick(event)
  EnsureState()
  local g = global

  if g.sensor_queue[g.sensor_index] ~= nil then
    local sensor = g.sensor_queue[g.sensor_index]

    for i, target in ipairs(SensorTargets(sensor)) do
      local name = target.name
      local products_finished = target.products_finished

      if target.valid and products_finished ~= nil and name ~= "datascience-collector" and name ~= "datascience-computer" then

        local unit_number = target.unit_number
        local cost = 0.5

        local recipe = target.get_recipe()
        if recipe ~= nil then
          cost = recipe.energy or 0.5
        end

        -- smelting recipes are less valuable than products
        if target.type == "furnace" then
          cost = cost * 0.25
        end

        local previous_products_finished = g.sensor_targets[unit_number] or products_finished

        if previous_products_finished < products_finished then
          -- something has been crafted
          g.data_points = g.data_points + ((products_finished - previous_products_finished) * cost)
          g.sensor_targets[unit_number] = products_finished
        else
          -- first encounter with target
          g.sensor_targets[unit_number] = previous_products_finished
        end
      end
    end
  end

  g.sensor_index = g.sensor_index < #g.sensor_queue and g.sensor_index+1 or 1

  local collector = g.collector_queue[g.collector_index]
  g.collector_index = g.collector_index < #g.collector_queue and g.collector_index+1 or 1

  local batch = math.floor(g.data_points / #g.collector_queue)

  if batch > 0 and collector ~= nil then
    g.data_points = g.data_points - collector.insert({
      name = "datascience-datum",
      count = batch,
    })
  end

  if collector ~= nil then
    if collector.is_connected_to_electric_network() then
      g.collector_radars[collector.unit_number].graphics_variation = 1
    else
      g.collector_radars[collector.unit_number].graphics_variation = 2
    end
  end

  local computer = g.computer_queue[g.computer_index]
  g.computer_index = g.computer_index < #g.computer_queue and g.computer_index+1 or 1

  if computer ~= nil then
    if computer.is_crafting() then
      g.computer_lights[computer.unit_number].graphics_variation = 1
    else
      g.computer_lights[computer.unit_number].graphics_variation = 2
    end
  end

  if g.data_points > #g.collector_queue*1000 then
    g.data_points = #g.collector_queue*1000
  end

  local rack = g.rack_queue[g.rack_index]
  g.rack_index = g.rack_index < #g.rack_queue and g.rack_index+1 or 1

  if rack ~= nil then
    if rack.is_crafting() then
      g.rack_displays[rack.unit_number].graphics_variation = 1
    else
      g.rack_displays[rack.unit_number].graphics_variation = 2
    end
  end

  if g.data_points > #g.collector_queue*1000 then
    g.data_points = #g.collector_queue*1000
  end

  --if game.tick % 60 == 0 then
  --  log("datascience data_points = "..g.data_points.." collectors = "..#g.collector_queue.." sensors = "..#g.sensor_queue)
  --end
end

script.on_init(function()
  --log("datascience on_init")
  script.on_event(defines.events.on_tick, OnTick)
  script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
  script.on_event({defines.events.on_pre_player_mined_item, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)

script.on_load(function()
  --log("datascience on_load")
  script.on_event(defines.events.on_tick, OnTick)
  script.on_event({defines.events.on_built_entity, defines.events.on_robot_built_entity}, OnEntityCreated)
  script.on_event({defines.events.on_pre_player_mined_item, defines.events.on_robot_pre_mined, defines.events.on_entity_died}, OnEntityRemoved)
end)
